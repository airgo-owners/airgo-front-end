import axios from 'axios';

const LOGIN_REST_API_URL = "http://localhost:8080/login"
const LOGOUT_REST_API_URL = "http://localhost:8080/logout"



class LoginService {

    getLogin() {
        return axios.get(LOGIN_REST_API_URL);
        
    }

    getLogout(){ 
        return axios.get(LOGOUT_REST_API_URL);
    }
}
export default new LoginService()