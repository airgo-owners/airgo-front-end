import React from 'react';


export default class FilterDeparatureLocationComponent extends React.Component {


    constructor() {
        super()
        this.state = { searchedValue: '' }
    }

    onSearch = (event) => {
        this.props.setFilter(event.target.value)
    }

    render() {
        return (
            <div className="input-group">
                <input type="text" className="form-control rounded" placeholder="Search" aria-label="Search"
                    aria-describedby="search-addon" onChange={this.onSearch} value={this.props.filter} />
            </div>

        )
    }

}