import React from 'react';
import FlightService from '../Services/FlightService'
import { ListGroup } from 'react-bootstrap';
import FlightCardComponent from './FlightCardComponent';
import Cookies from 'js-cookie';
import Navigator from './Navigator';
import FilterDeparatureLocationComponent from './FilterDeparatureLocationComponent';
import FilterArrivalLocationComponent from './FilterArrivalLocationComponent'
import FilterDeparatureDateComponent from './FilterDepartureDateComponent';
import Container from 'react-bootstrap/Container';


export default class Airgofrontpage extends React.Component {

    constructor(props) {
        super(props)
        this.state = {

            flights: [],
            searchTerm: '',
            searchTerm2: ''





        }
        this.setFilter = this.setFilter.bind(this)
        this.setFilter2 = this.setFilter2.bind(this)
        this.setFilter3 = this.setFilter3.bind(this)

    }




    componentDidMount() {
        FlightService.getFlight().then((response) => {
            this.setState({ flights: response.data })

        })
    }

    setFilter(newFilter) {
        this.setState({
            searchTerm: newFilter

        })
    }

    setFilter2(newFilter) {
        this.setState({
            searchTerm2: newFilter
        })
    }

    setFilter3(newFilter) {
        this.setState({
            searchDepDate: newFilter
        })
    }




    render() {


        Cookies.set('foo', 'bar')
        console.log(Cookies.get('JSESSIONID'))
        console.log(Cookies.get())
        console.log(this.state.flights)
        return (


            <div className="background-for-main">
                <Navigator />
                <h1 className="slogan">
                    <strong>AirGo</strong>
                    <h2 className="text-slogan">Explore the world with ease</h2>
                </h1>
                <Container className="filter-container">
                    <FilterDeparatureLocationComponent className="departure-search-bar" setFilter={this.setFilter} filter={this.state.searchTerm} ></FilterDeparatureLocationComponent>
                    <FilterArrivalLocationComponent className="arrival-search-bar" setFilter={this.setFilter2} filter={this.state.searchTerm2} ></FilterArrivalLocationComponent>
                    <FilterDeparatureDateComponent className="date-search-bar" setFilter={this.setFilter3} filter={this.state.searchDepDate}></FilterDeparatureDateComponent>
                </Container>

                <ListGroup>
                    {

                        this.state.flights
                            .filter(
                                flight => {
                                    if (this.state.searchTerm === '' && this.state.searchTerm2 === '') { return false }
                                    return (

                                        flight.departureLocation.toLowerCase().includes(this.state.searchTerm.toLowerCase()) &&
                                        flight.arrivalLocation.toLowerCase().includes(this.state.searchTerm2.toLowerCase()) &&
                                        flight.departureDate.includes(this.state.searchDepDate)


                                    )
                                }
                            )
                            .map(
                                flight =>
                                    <ListGroup.Item key={flight.id} className="list-group-item">
                                        <FlightCardComponent flightcard={flight} />
                                    </ListGroup.Item>
                            )
                    }
                </ListGroup>

                <section class="section padding-top-70 padding bottom-0" className="text-box-class" >
                    <div >
                        <div class="header">
                            <h3 >
                                All our top travel offers, deals, insider tips and inspiration
                            </h3>
                        </div>
                        <div class="text">
                            <p>
                                Hello, there. Let us help you start your search for cheap airfare and travel deals. Whether it’s a romantic weekend getaway, family vacation, bucket-list trip to your dream destination, or a business trip with a side of fun, we have you covered. Finding cheap airline tickets for domestic and international flights is what we do best.
                        </p>

                        </div>
                    </div>
                </section>

            </div>
        )
    }



}